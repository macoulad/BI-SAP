; definice pro nas typ procesoru
.include "m169def.inc"
; podprogramy pro praci s displejem
.org 0x1000
.include "print.inc"

; Zacatek programu - po resetu
.org 0
	  
jmp start

; Zacatek programu - hlavni program
.org 0x100

start:
    ; Inicializace zasobniku
	ldi r16, 0xFF
	out SPL, r16
	ldi r16, 0x04
	out SPH, r16
    ; Inicializace displeje
	call init_disp
    
		.cseg
		.org 0x110
slovo:	.db "Ukaz prsa veroniko",0	



begin:
			
ldi r30, low(2*slovo)	; dolni bajt bajtove adresy retezce (registr R30 = dolni bajt 16ti bitoveho registru Z)
ldi r31, high(2*slovo)	; horni bajt bajtove adresy retezce (registr R31 = horni bajt 16ti bitoveho registru Z)
clr r28
ldi r20,0
	
	
mov r21,r20	
cycle:
	mov r26,r25			;posun textu
	mov r25,r24
	mov r24,r23
	mov r23,r22
	mov r22,r21

	lpm	r21,Z+ 			;nacti znak

	cpi r21,0xE0		;pokud neni 0 pokracuj dal
	breq endcycle		;pokud je ukoncen 0 zacni znovu
	
	call show			;zobraz znaky
	call wait_timer
		
	inc r28				;posun pointer
	jmp cycle			;loop

endcycle:
	clr r28	


konec:
rjmp begin


show:	
		ldi r17,1
		mov r16,r26
		inc r17
		call show_char
		 
		mov r16,r25
		inc r17
		call show_char

		mov r16,r24
		inc r17
		call show_char

		mov r16,r23
		inc r17
		call show_char

		mov r16,r22
		inc r17
		call show_char

		mov r16,r21
		inc r17
		call show_char

		ret

wait_timer :
	ldi r19, 255
	ldi r18, 255
	call Delay_1sec
	dec r19
	cpi	r19, 0
	breq wait_timer
	ret 

Delay_1sec: 
	
	ldi r19, 0xFF
	Delay_2sec:     
		dec r19
		cpi	r19, 0
		brne Delay_2sec    
		
	dec r18
	cpi	r18, 0
	brne Delay_1sec
	ret 

		

