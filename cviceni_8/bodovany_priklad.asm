; definice pro nas typ procesoru
.include "m169def.inc"
; podprogramy pro praci s displejem
.org 0x1000
.include "print.inc"

; Zacatek programu - po resetu
.org 0
jmp start

; Zacatek programu - hlavni program
.org 0x100
start:
    ; Inicializace zasobniku
   ldi r16, 0xFF
   out SPL, r16
   ldi r16, 0x04
   out SPH, r16
    ; Inicializace displeje
   call init_disp

   ; *** ZDE muzeme psat nase instrukce

	ldi 	r21, 55
	ldi 	r22, 3	//prvni pozice
	ldi 	r23, 2	//druha pozice
	ldi 	r24, '0'	
   	ldi		r18, 0xAA
   	mov		r19, r18 //zaloha hodnoty na preklad do hexa
   	mov 	r20, r19
   	andi 	r20, 0x0F //spodni bity
	lsr		r19
   	lsr 	r19	 
   	lsr		r19
   	lsr 	r19		//horni bity

   	cpi 	r20,10
   	brsh write_char
   	mov		r16, r20
	add		r16, r24
	mov 	r17, r22
	call 	show_char
   	jmp second_char
   	
write_char:
   	mov   	r16, r20
	mov 	r17, r22
   	add   	r16, r21
	call 	show_char

second_char:
   	cpi		r19, 10
   	brsh 	write_char2
	mov		r17, r23
	mov 	r16, r19
	add 	r16, r24
   	call   show_char

write_char2:
	mov 	r17, r23
   	mov 	r16, r19
   	add   	r16, r21
   	call  show_char


; Zastavime program - nekonecna smycka
   rjmp PC
