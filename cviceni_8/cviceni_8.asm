; definice pro nas typ procesoru
.include "m169def.inc"
; podprogramy pro praci s displejem
.org 0x1000
.include "print.inc"

; Zacatek programu - po resetu
.org 0
jmp start

; Zacatek programu - hlavni program
.org 0x100
start:
    ; Inicializace zasobniku
	ldi r16, 0xFF
	out SPL, r16
	ldi r16, 0x04
	out SPH, r16
    ; Inicializace displeje
	call init_disp

	; *** ZDE muzeme psat nase instrukce
	LDI R16, 5
	LDI R17, 10
	LDI R18, 58

	LSL R16
	LSL R16  			// R16 = 4XR16

	MOV R19, R17
	LSL R17
	ADD R17, R19		// R17 = 3XR17

	ADD R20, R16
	ADD R20, R17
	SUB R20, R18 

	ASR R20
	ASR R20
	ASR R20

	; Zastavime program - nekonecna smycka
	rjmp PC
